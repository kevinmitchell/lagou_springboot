package com.lagou.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lagou.mapper.TArticleMapper;
import com.lagou.pojo.TArticle;
import com.lagou.service.TArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TArticleServiceImpl implements TArticleService {

    @Autowired
    private TArticleMapper tArticleMapper;

    @Override
    public PageInfo<TArticle> queryTArticle(int page, int pageSize) {
        PageHelper.startPage(page, pageSize);
        List<TArticle> articleList = tArticleMapper.queryTArticle();
        PageInfo<TArticle> info = new PageInfo<>(articleList);
        return info;
    }

}
