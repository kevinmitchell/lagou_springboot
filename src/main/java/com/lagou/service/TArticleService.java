package com.lagou.service;

import com.github.pagehelper.PageInfo;
import com.lagou.pojo.TArticle;

public interface TArticleService {

    PageInfo<TArticle> queryTArticle(int page, int pageSize);
}
