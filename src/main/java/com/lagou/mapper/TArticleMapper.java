package com.lagou.mapper;

import com.lagou.pojo.TArticle;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TArticleMapper {

    List<TArticle> queryTArticle();
}
