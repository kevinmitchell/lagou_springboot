package com.lagou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootRun {


    public static void main(String[] args) {
        SpringApplication.run(SpringbootRun.class, args);
    }

}
