package com.lagou.controller;

import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import com.lagou.pojo.TArticle;
import com.lagou.service.TArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ArticleController {

    @Autowired
    private TArticleService tArticleService;

    @RequestMapping("/index")
    public String index(Model model, HttpServletRequest request) {

        String currentPage = request.getParameter("currentPage");
        String pageSize = request.getParameter("pageSize");
        int current = 1;
        int size = 2;
        if (StringUtil.isNotEmpty(currentPage)) {
            current = Integer.valueOf(currentPage);
        }
        if (StringUtil.isNotEmpty(pageSize)) {
            size = Integer.valueOf(pageSize);
        }
        PageInfo<TArticle> tArticles = tArticleService.queryTArticle(current, size);
        model.addAttribute("pageInfo", tArticles);

        return "client/index";
    }
}
